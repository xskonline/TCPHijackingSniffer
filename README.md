# TCP Hijacking Sniffer
----------
TCP旁路劫持的检测、取证软件。

## 使用说明

因为本软件需要抓取数据包，所以要先安装不少东西。

首先下载依赖软件环境 [pre-install.zip](http://git.oschina.net/xskonline/TCPHijackingSniffer/attach_files) ，再下载ZIP版本的 [Scapy](http://www.secdev.org/projects/scapy/) 。

按下面的顺序依次安装：
- python-2.7.8.msi
- 解压Scapy，在对应目录下执行`python setup.py install`完成安装
- pywin32-219.win32-py2.7.exe
- WinPcap_4_1_3.exe
- pcap-1.1.win32-py2.7.exe
- dnet-1.12.win32-py2.7.exe
- pyreadline-2.0.win32.exe

对应目录下运行`python tcphijack.py`。

## 授权

本软件遵循[Mozilla Public License, version 2.0](http://mozilla.org/MPL/2.0/)开源协议。